
shinyServer(function(input, output) {

 # création des listes de choix------------------------------------
    
    #nommage du résultat du choix du millesime
    millesime<-reactive({
      mill <- input$mill
    })
    
    #nommage des résultats du filtre région
    region_code<-reactive({
        reg <- as.character(regions$REG[match(input$ma_reg,regions$NOM_REG)])
    })
    region_libelle<-reactive({
        reg<- input$ma_reg
    })
    
    # création liste codes des départements
    liste_dep <- reactive({
        as.character(list_dep_in_reg(region_code()))
        })
  
    # création liste noms des départements
    liste_dep2 <- reactive({
        as.character(departements$NOM_DEP[match(liste_dep(),departements$DEP)])
    })
 
    # Création liste des départements
    output$dep_selection <-renderUI({
        selectInput(inputId ="dep_selection",label="S\u00e9lectionner un d\u00e9partement",
                    choices = liste_dep2()
        )    
        })    

    #nommage des résultats du filtre département
    departement_code<-reactive({
        dep<- as.character(departements$DEP[match(input$dep_selection,departements$NOM_DEP)])
    })
    departement_libelle<-reactive({
        dep<- input$dep_selection
    })   
    
    # création liste codes des epci
    liste_epci <- reactive({
        as.character(list_epci_in_dep(departement_code()))
    })
    
    # création liste noms des epci
    liste_epci2 <- reactive({
        as.character(epci$NOM_EPCI[match(liste_epci(),epci$EPCI)])
    })
    
    # Création liste des epci
    output$epci_selection <-renderUI({
        selectInput(inputId ="epci_selection",label="S\u00e9lectionner un EPCI",
                    choices = liste_epci2()
        )
    })    
    
    #nommage des résultats du filtre EPCI
    epci_code<-reactive({
        epci<- as.character(epci$EPCI[match(input$epci_selection,epci$NOM_EPCI)])
    })
    epci_libelle<-reactive({
        epci<- input$epci_selection
    })
    
    # renvoi du code EPCI
    output$texte1 <-renderText({ 
      epci_code()
    })

# Fonction de création des datamart filtré
creer_cube_00 <- function (x){
  cube <- x %>%
    # filter (annee == millesime(),grepl(region_code(), REG))
    filter (annee == millesime(), REG != "NULL")
  # condition sur la région
  cube <- if (input$valid_reg == "reg0"){  # si selection "aucune r\u00e9gion"
    cube %>% filter( TypeZone != "REG")    # on enlève les lignes régions
  } else {
    # cube }                                  #sinon on garde tout
    cube  %>% filter( TypeZone != "REG" | REG == region_code()) }   #sinon on garde ce qui n'est pas region et la region selectionnee

  # condition sur le département
  cube <- if (input$valid_dep == "dep0"){   #si selection "aucun d\u00e9partement"
    cube %>% filter( TypeZone != "DEP")     # on enlève les lignes département
  } else if (input$valid_dep == "dep1"){    #si selection que département sélectionné
    cube %>% filter( TypeZone != "DEP" | CodeZone == departement_code())   # on garde ce qui n'est pas département ou la ligne du département
  } else {                                  #tous les departements
    cube %>% filter( TypeZone != "DEP" | grepl(region_code(), REG)) }     #sinon on garde ce qui n'est pas departement ou les departements de la region
  
  # condition sur l'EPCI
  cube <- if (input$valid_epci == "epci0"){
    cube %>% filter( TypeZone != "EPCI")
  } else if (input$valid_epci == "epci1"){    #uniquement l'epci selectionne
    cube %>% filter( TypeZone != "EPCI" | CodeZone == epci_code())  
    # cube %>% filter( TypeZone != "EPCI" | grepl(epci_code(), CodeZone) )
  } else if (input$valid_epci == "epci2"){   #tous les epci du departement
    cube %>% filter( TypeZone != "EPCI" | grepl(departement_code(), DEP))  #tous les epci du département
  } else {   #tous les epci de la region
    cube %>% filter( TypeZone != "EPCI" | grepl(region_code(), REG))}  #sinon on garde ce qui n'est pas de l'epci ou les epci de la region
    
  # condition sur la commune
  cube <- if (input$valid_com == "com0"){
    cube %>% filter( TypeZone != "COM")
  } else if (input$valid_com == "com1"){
    cube %>% filter( TypeZone != "COM" | EPCI == epci_code() ) #toutes les communes de l'EPCI
  } else if (input$valid_com == "com2"){
    cube %>% filter( TypeZone != "COM" | grepl(departement_code(), DEP)) #toutes les communes du département
  } else {
    cube %>% filter( TypeZone != "COM" | grepl(region_code(), REG))}  #toutes les communes de la region
  
  cube <- cube  %>% select(-DEP,-REG) #Enlever des colonnes qui comportent des listes et empechent l'export en csv
}

#tableau-------------------------------------------------------------------------------------------

# # cube sélectionné
cube_selection<-reactive({
  cube<- df_liste[input$mon_cube] %>% as.data.frame() %>%
    rename_with(~gsub(input$mon_cube,"",.x)) %>%
    rename_with(~gsub("X.","",.x))
})

cube00 <- reactive({
  creer_cube_00(cube_selection())
})

# Renvoi du tableau
output$tableau = DT::renderDataTable(
  cube00(),
  options = list(scrollX = TRUE)
)

#téléchargements-------------------------------------------------------------------------------------------

# numero et libelle du cube
num_cube <- reactive({
  input$mon_cube
})
libelle_cube <- reactive({
  tableau_liste_num  %>%
    filter (x == num_cube()) %>%
    pull(y)
})

# renvoi du nom du cube
output$libelle_cube <-renderText({ 
  libelle_cube()
})


#exportation des données    
output$download_cubes <-
  downloadHandler(
  filename = function() {
    paste("cube ",millesime(),"-",input$mon_cube,".csv", sep="")
  },
  content = function(file) {
    # cube000 <- creer_cube_00(cube_00())
    cube000 <- cube00()
    write.table(cube000,file,row.names=FALSE,quote=FALSE, sep = ";",fileEncoding = "UTF-8")
  }
)

# telechargement du PDF
output$download_pdf <- downloadHandler(
  filename = "filocom_description_cubes.pdf",
  content = function(file) {
    file.copy("www/filocom_description_cubes_variables_regles_secret.pdf", file)
  } )   
    
    
    
})
