Application de simplification de la réponse à la demande statistique    
Code source de l'application : https://gitlab.com/dreal-datalab/reponse_demande_statistique   
Lien vers l'application (intranet) : http://set-pdl-dataviz.dreal-pdl.ad.e2.rie.gouv.fr/reponse_demande_statistique/

Le but de l'application reponse_demande_statistique, est de faciliter la production de données statistiques, en simplifiant au maximum l'extraction des données.



<div class = "row">
<div class = "col-md-4">
<img style="float: left;margin:5px 20px 5px 0px" src="www/imp_ecran1.jpg" width="100%">
</div>
<div class = "col-md-8">
La page **Filocom** met à disposition les données disponibles sur le site intranet Beyond ( http://10.112.4.4/wds_2/ ) . Les données secrétisées sont reprises au même format, sans modifications. L'intérêt principal de l'application réside dans la possibilité de choisir très rapidement ses zonages qui peuvent être exportés dans un fichier unique.
</div>
</div>
